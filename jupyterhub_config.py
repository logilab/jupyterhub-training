# Configuration file for jupyterhub.
import os

#  Users should be properly informed if this is enabled.
c.JupyterHub.admin_access = True

# spawner
c.JupyterHub.spawner_class = 'dockerspawner.DockerSpawner'
c.DockerSpawner.image = 'voila-demo'
c.DockerSpawner.use_internal_ip = True
c.DockerSpawner.remove_containers = True
c.Spawner.mem_limit = '4G'
c.JupyterHub.template_paths=['/srv/']

c.JupyterHub.log_level = 'DEBUG'
c.Spawner.debug = True
c.DockerSpawner.debug = True
c.DockerSpawner.default_url = '/lab'

# execute entry-point before launch
spawn_cmd = os.environ.get('DOCKER_SPAWN_CMD', "start-singleuser.sh")
c.DockerSpawner.extra_create_kwargs.update({ 'command': spawn_cmd })

# network
network_name = 'jupyterhub-training_jupyterhub'
c.DockerSpawner.use_internal_ip = True
c.DockerSpawner.network_name = network_name
# Pass the network name as argument to spawned containers
c.DockerSpawner.extra_host_config = { 'network_mode': network_name }
# User containers will access hub by container name on the Docker network
c.JupyterHub.hub_ip = 'jupyterhub'
c.JupyterHub.hub_port = 8008

#  - You can set this to `/lab` to have JupyterLab start by default, rather than Jupyter Notebook.
c.Spawner.default_url = '/lab'

#  Defaults to an empty set, in which case no user has admin access.
c.Authenticator.admin_users = {'ogiorgis', 'admin'}

# DummyAuthenticator
c.JupyterHub.authenticator_class = 'jupyterhub.auth.DummyAuthenticator'

# notebooks
notebook_dir = '/home/jovyan/'
c.Spawner.notebook_dir = notebook_dir
c.DockerSpawner.notebook_dir = notebook_dir

# voila service
c.ConfigurableHTTPProxy.auth_token = 'blahblahblah'
voila_service_dict = {
    'PROXY_TOKEN': c.ConfigurableHTTPProxy.auth_token,
    'PROXY_API_URL': 'http://%s:%d/' % ("127.0.0.1", 8000)
}
# voila_service_dict.update(os.environ)
c.JupyterHub.services.append(
    {
        'name': 'voila',
        'command': ['bash', '-c', 'jupyter_voila_service'],
        'environment': voila_service_dict,
    }
)
