# docker build -t jlh .
# docker run -d -p 8000:8000 jlh
FROM jupyterhub/jupyterhub:1.0.0

# Jupyterlab-training
RUN apt-get update && apt-get -y upgrade
RUN apt-get -y install python3-pip

# Docker
RUN apt-get -y install apt-transport-https ca-certificates curl gnupg2
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
RUN apt-get -y install software-properties-common # apt-transport-https
RUN add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian buster stable"
RUN apt-get update
RUN apt-get -y install docker-ce

# jupyterhub
RUN pip install notebook jupyter dockerspawner jupyterhub-ldapauthenticator pyzmq==17 jupyter_client
RUN pip install git+https://github.com/afeiszli/jupyter-voila-extension.git

# jupyterhub config
COPY homepage/login.html /srv/login.html
COPY jupyterhub_config.py /home/jovyan/jupyterhub_config.py
