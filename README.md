# Build jupyterhub image

```sh
docker-compose build
```

# Run jupyterhub

```sh
docker-compose up
```

# jupyterlab-training image

To build de jupyterlab image see
- https://gitlab.com/logilab/jupyterlab-training-docker/
- and https://gitlab.com/logilab/exercism_workspace


visit http://0.0.0.0:8000/hub/login

# Author

<table class="none">
<tr>
<td>
  <img src="https://assets.gitlab-static.net/uploads/-/system/group/avatar/3681849/logo.png" width="128">
</td>
<td>
    https://www.logilab.fr
</td>
</tr>
</table>

# Licence
[![Licence: LGPL v3](https://img.shields.io/badge/License-LGPL%20v3-blue.svg)](https://www.gnu.org/licenses/lgpl-3.0)

# Acknowledgements

<table class="none">
<tr>
<td>
  <img src="http://opendreamkit.org/public/logos/Flag_of_Europe.svg" width="128">
</td>
<td>
  This package was created as part of the Horizon 2020 European
  Research Infrastructure project
  <a href="https://opendreamkit.org/">OpenDreamKit</a>
  (grant agreement <a href="https://opendreamkit.org/">#676541</a>).
</td>
</tr>
</table>